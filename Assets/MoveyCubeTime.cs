﻿using System.Collections;
using UnityEngine;

public class MoveyCubeTime : MonoBehaviour
{
	//This will show up in the inspector because it's public
	public float speed = 10;


	// Update is called once per frame
	void Update()
	{
		//store your left/right input and multiply it by speed
		float xInput = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
		//store your up/down input and multiply it by speed
		float yInput = Input.GetAxis("Vertical") * speed * Time.deltaTime;

		//affect your transform with these inputs
		transform.Translate(xInput, yInput, 0);
	}

} // thanks ben i suck